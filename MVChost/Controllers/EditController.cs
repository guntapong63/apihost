﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class EditController : Controller
    {
        // GET: Edit
        public string editdata(int id, string Fname, string Lname, string gender, string position)
        {
            var client = new HttpClient();
            var result = client.PostAsJsonAsync("http://localhost:44334/api/Edit", JsonConvert.SerializeObject
            (new
            {
                id = id,
                em_Fname = Fname,
                em_Lname = Lname,
                em_gender = gender,
                em_position = position


            })).Result;
            return result.Content.ReadAsStringAsync().Result;
        }

    }
}