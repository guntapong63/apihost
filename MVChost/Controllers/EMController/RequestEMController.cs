﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class RequestEMController : Controller
    {
        // GET: EM
        public ActionResult Testpage(int id=0)
        {
            var client = new HttpClient();
            var request = client.PostAsJsonAsync("http://localhost:44334/api/Request", JsonConvert.SerializeObject
            (new
            {
            id= id
            })).Result;

            if (request.IsSuccessStatusCode)
            {
                var result2 = request.Content.ReadAsStringAsync().Result;
                ViewBag.request = result2;
            }
            else
            {
                ViewBag.error = "CODE_ERROR:003";
            }

            ViewBag.id = id;

            return View();
        }
    }
}