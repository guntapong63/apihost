﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class SubmitEMController : Controller
    {
        // GET: SubmutEM
        public string submitdata(string Fname, string Lname, string gender, string position, int id = 0)
        {
            var client = new HttpClient();
            var DataJson = JsonConvert.SerializeObject(new
            {
                id = id
            });

          
            var response = client.PostAsJsonAsync("http://localhost:44334/api/Submit", JsonConvert.SerializeObject
            (new
            {
                id = id,
                em_Fname = Fname,
                em_Lname = Lname,
                em_gender = gender,
                em_position = position

            })).Result;

            return "";
        }


    }
}