﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class SubmitController : Controller
    {
        // GET: Submit
        public string submitdata(string Fname, string Lname, string gender, string position)
        {
            var client = new HttpClient();
            var response = client.PostAsJsonAsync("http://localhost:44334/api/Submit", JsonConvert.SerializeObject
            (new
            {
                em_Fname = Fname,
                em_Lname = Lname,
                em_gender = gender,
                em_position = position

            })).Result;
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}