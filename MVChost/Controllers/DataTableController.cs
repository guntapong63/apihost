﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class DataTableController : Controller
    {
        // GET: DataTable
        public ActionResult DataTable()
        {
            var client = new HttpClient();

            int sEcho = int.Parse(Request.Form["sEcho"]);
            int iDisplayStart = int.Parse(Request.Form["iDisplayStart"]);
            int iDisplayLength = int.Parse(Request.Form["iDisplayLength"]);
            int iSortingCols = int.Parse(Request.Form["iSortCol_0"]);
            string sSearch = Request.Form["sSearch"].ToString();
            //string sSearch = Request["parameter1"];;

            // สร้างไฟล์ Json
            var DataJson = JsonConvert.SerializeObject(new
            {
                sEcho = sEcho,
                start = iDisplayStart,
                lenght = iDisplayLength,
                sort = iSortingCols,
                search = sSearch
            });

            var res = client.PostAsJsonAsync("http://localhost:44334/api/DataTable", DataJson).Result;
            var result = res.Content.ReadAsStringAsync().Result;
            return Json(result);
        }
    }
}