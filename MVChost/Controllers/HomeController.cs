﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
            {
            var client = new HttpClient();
            var request = client.PostAsJsonAsync("http://localhost:44334/api/Request", JsonConvert.SerializeObject
            (new
            { })).Result;

            if (request.IsSuccessStatusCode)
            {
                var result2 = request.Content.ReadAsStringAsync().Result;
                ViewBag.request = result2;
            }
            else
            {
                ViewBag.error = "CODE_ERROR:003";
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult EditbyID()
        {
            ViewBag.Massage = "HAHA";
                return View();
        }
        public ActionResult Testpage()
        {
            ViewBag.Massage = "test";
            return View();
        }
    }
}