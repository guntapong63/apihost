﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MVChost.Controllers
{
    public class DeleteController : Controller
    {
        // GET: Delete
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public string deletedata(int Id)
        {
            var client = new HttpClient();
            var result = client.PostAsJsonAsync("http://localhost:44334/api/Delete", JsonConvert.SerializeObject
                (new
                {
                    id = Id
                })).Result;
            return result.Content.ReadAsStringAsync().Result;
        }
    }
}